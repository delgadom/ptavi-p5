***** Análisis de una sesión SIP

Se ha capturado una sesión SIP con Ekiga (archivo sip.cap.gz), que se puede abrir con Wireshark. Se pide rellenar las cuestiones que se plantean en este guión en el fichero p5.txt que encontrarás también en el repositorio.

  * Observa que las tramas capturadas corresponden a una sesión SIP con Ekiga, un cliente de VoIP para GNOME. Responde a las siguientes cuestiones:
    * ¿Cuántos paquetes componen la captura?

      954 paquetes componen la captura.

    * ¿Cuánto tiempo dura la captura?

      La captura tiene una duración de 56.149345 segundos

    * ¿Qué IP tiene la máquina donde se ha efectuado la captura? ¿Se trata de una IP pública o de una IP privada? ¿Por qué lo sabes?

      La dirección desde donde se ha efectuado la captura es la 192.168.1.34. Esta dirección corresponde a una privada ya que está en el rango comprendido entre 192.168.0.0 – 192.168.255.255.

  * Antes de analizar las tramas, mira las estadísticas generales que aparecen en el menú de Statistics. En el apartado de jerarquía de protocolos (Protocol Hierarchy) se puede ver el porcentaje del tráfico correspondiente al protocolo TCP y UDP.
    * ¿Cuál de los dos es mayor? ¿Tiene esto sentido si estamos hablando de una aplicación que transmite en tiempo real?

      Es mayor el correspondiente a UDP con un 96.2 % de los paquetes frente a un 2.1 % de TCP, esto tiene sentido ya que para enviar paquetes en tiempo real se utiliza UDP y no TCP.

    * ¿Qué otros protocolos podemos ver en la jerarquía de protocolos? ¿Cuales crees que son señal y cuales ruido?

      Podemos ver los siguientes protocolos. Correspondiente a la señal: udp, sip, rtp. Correspondiente a ruido: icmp, dns, arp, http, ipv4.

  * Observa por encima el flujo de tramas en el menú de Statistics en IO Graphs. La captura que estamos viendo incluye desde la inicialización (registro) de la aplicación hasta su finalización, con una llamada entremedias.
    * Filtra por sip para conocer cuándo se envían paquetes SIP. ¿En qué segundos tienen lugar esos envíos?

      Se envían los paquetes SIP en estos intervalos de tiempo: (6,8) U (13,15) U (15,17) U (37,40) U (54, 56.14)

    * Y los paquetes con RTP, ¿cuándo se envían?

      Los paquetes RTP se envían desde el segundo 16 al 37

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Analiza las dos primeras tramas de la captura.
    * ¿Qué servicio es el utilizado en estas tramas?

      El servicio es DNS

    * ¿Cuál es la dirección IP del servidor de nombres del ordenador que ha lanzado Ekiga?

      La dirección es la 80.58.61.250

    * ¿Qué dirección IP (de ekiga.net) devuelve el servicio de nombres?

      La dirección que devuelve el servicio de nombres es la 86.64.126.35

  * A continuación, hay más de una docena de tramas TCP/HTTP.
    * ¿Podrías decir la URL que se está pidiendo?

      La url pedida es http://ekiga.net/ip/

    * ¿Qué user agent (UA) la está pidiendo?

      User-Agent: Ekiga\r\n

    * ¿Qué devuelve el servidor?

      Los datos correspondientes a:
      Line-based text data: text/html (1 lines)
      83.36.48.212

    * Si lanzamos el navegador web, por ejemplo, Mozilla Firefox, y vamos a la misma URL, ¿qué recibimos? ¿Qué es, entonces, lo que está respondiendo el servidor?

      Yo recibo la dirección 83.52.71.171

  * Hasta la trama 45 se puede observar una secuencia de tramas del protocolo STUN.
    * ¿Por qué se hace uso de este protocolo?

      Un servidor STUN permite a los clientes que se conecten a un servidor alojado en una red local distinta, en donde el cliente tiene que pasar a través de un NAT (firewall, router, etc.).

    * ¿Podrías decir si estamos tras un NAT o no?

      Estamos tras una NAT ya que esta es necesaria para el protocolo STUN

  * La trama 46 es la primera trama SIP. En un entorno como el de Internet, lo habitual es desconocer la dirección IP de la otra parte al realizar una llamada. Por eso, todo usuario registra su localización en un servidor Registrar. El Registrar guarda información sobre los usuarios en un servidor de localización que puede ser utilizado para localizar usuarios.
    * ¿Qué dirección IP tiene el servidor Registrar?

      Tiene la dirección 86.64.162.35

    * ¿A qué puerto (del servidor Registrar) se envían los paquetes SIP?

      Sent-by port: 5063

    * ¿Qué método SIP utiliza el UA para registrarse?

      El UA utiliza el método REGISTER

    * Además de REGISTER, ¿podrías decir qué instrucciones SIP entiende el UA?

      INVITE, ACK, CANCEL, OPTIONS, PRACK, SUBSCRIBE, NOTIF,Y REFER, PUBLISH, UPDATE, MESSAGE, INFO.

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Fijémonos en las tramas siguientes a la número 46:
    * ¿Se registra con éxito en el primer intento?

      No se registra con éxito, mas adelante en la trama 50 nos devuelve un unauthorized

    * ¿Cómo sabemos si el registro se ha realizado correctamente o no?

      Porque nos tiene que responder con una autorización de registro, correspondiente a un 200 OK

    * ¿Podrías identificar las diferencias entre el primer intento y el segundo de registro? (fíjate en el tamaño de los paquetes y mira a qué se debe el cambio)

      El tamaño de los paquetes es inferior en el primero ya que no envía el campo correspondiente a authorization a diferencia del segundo intento que si se envía.

    * ¿Cuánto es el valor del tiempo de expiración de la sesión? Indica las unidades.

      expires=3600. Esto significa que el tiempo de expiración es de 3600 segundos.


  * Una vez registrados, podemos efectuar una llamada. Vamos a probar con el servicio de eco de Ekiga que nos permite comprobar si nos hemos conectado correctamente. El servicio de eco tiene la dirección sip:500@ekiga.net. Veamos el INVITE de cerca.
    * ¿Puede verse el nombre del que efectúa la llamada, así como su dirección SIP?

    From: "Gregorio Robles" <sip:grex@ekiga.net>;tag=621be956-b29e-db11-82ee-0013ce8c7c2b

    * ¿Qué es lo que contiene el cuerpo de la trama? ¿En qué formato/protocolo está?

      Tiene la informacion del usuario que establece la llamada en el formato SDP

    * ¿Tiene éxito el primer intento? ¿Cómo lo sabes?

      No tiene éxito el primer intento ya que nos devuelve este valor en la trama 85. 85	14.599871	86.64.162.35	192.168.1.34	SIP	744	Status: 407 Proxy Authentication Required |

    * ¿En qué se diferencia el segundo INVITE más abajo del primero? ¿A qué crees que se debe esto?

      En que rellena los datos de autentificación:
      Proxy-Authorization: Digest username="grex", realm="ekiga.net", nonce="45a439f6ba5f681cd945fb7c40529fd1e0f28a81", uri="sip:500@ekiga.net", algorithm=md5, response="9a5627c14a8a433bd308d2ec54777240"


  * Una vez conectado, estudia el intercambio de tramas.
    * ¿Qué protocolo(s) se utiliza(n)? ¿Para qué sirven estos protocolos?

      Al empezar la llamada tenemos algunos paquetes DNS mas adelante tenemos H.261 y RTP que estos son un estandar en la comunicación de audio y video.

    * ¿Cuál es el tamaño de paquete de los mismos?

      El tamaño de los paquetes para H.261 es de entre 1000 y 1100 en cambio los correspondientes a RTP son de 214

    * ¿Se utilizan bits de padding?

      No ya que tiene el valor de este campo en false.

    * ¿Cuál es la periodicidad de los paquetes (en origen; nota que la captura es en destino)?

      La periodicidad de paquetes es de entre 0.02 y 0.03 segundos

    * ¿Cuántos bits/segundo se envían?

      214 * 8 = 1712/0.025 = 68480 bits/segundo

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Vamos a ver más a fondo el intercambio RTP. En Telephony hay una opción RTP. Empecemos mirando los flujos RTP.
    * ¿Cuántos flujos hay? ¿por qué?

      Tenemos dos flujos, uno para RTP y el otro para H.261 este ultimo se utliza para video

    * ¿Cuántos paquetes se pierden?

      Ninguno. lost 0.0%

    * ¿Cuál es el valor máximo del delta? ¿Y qué es lo que significa el valor de delta?

      Depende de:
      RTP = 1,29044 segundos
      H.261 = 1,29047 segundos

    * ¿Cuáles son los valores de jitter (medio y máximo)? ¿Qué quiere decir eso? ¿Crees que estamos ante una conversación de calidad?

      Max Jitter: 119.64 ms. Mean Jitter: 42.50 ms estos valores corresponden a una llamada de una fluidez buena.

  * Elige un paquete RTP de audio. Analiza el flujo de audio en Telephony -> RTP -> Stream Analysis.
    * ¿Cuánto valen el delta y el jitter para el primer paquete que ha llegado?

      Delta = 0.
      Jitter = 0.

    * ¿Podemos saber si éste es el primer paquete que nos han enviado?

      Podemos saber que es el primer paquete por el valor de la delta de antes

    * Los valores de jitter son menores de 10ms hasta un paquete dado. ¿Cuál?

      En el paquete 247

    * ¿A qué se debe el cambio tan brusco del jitter?

      Se debe al cambio que sufre la delta

    * ¿Es comparable el cambio en el valor de jitter con el del delta? ¿Cual es más grande?

      Como he dicho antes si podemos compararlo pero es mayor el valor que tenemos en la delta

  * En Telephony selecciona el menú VoIP calls. Verás que se lista la llamada de voz IP capturada en una ventana emergente. Selecciona esa llamada y pulsa el botón Play Streams.
    * ¿Cuánto dura la conversación?

      Dura aproximadamente 20.9 segundos de 17.2 a 38.1

    * ¿Cuáles son sus SSRC? ¿Por qué hay varios SSRCs? ¿Hay CSRCs?

      Tenemos dos SSRC:
      0xbf4afd37
      0x43306582
      No hay CSRCs


  * Identifica la trama donde se finaliza la conversación.
    * ¿Qué método SIP se utiliza?

      Se utiliza el metodo BYE que aparece en la trama 924

    * ¿En qué trama(s)?

      Empieza en la 924 hasta la 933

    * ¿Por qué crees que se envía varias veces?

      Porque está esperando la respuesta de 200 OK y como no se la devuelve sigue enviándolo.


  * Finalmente, se cierra la aplicación de VozIP.
    * ¿Por qué aparece una instrucción SIP del tipo REGISTER?

      Para quitarle del registro y cerrar la conversación.

    * ¿En qué trama sucede esto?

      Esto ocurre en la trama 950

    * ¿En qué se diferencia con la instrucción que se utilizó con anterioridad (al principio de la sesión)?

      Se diferencia que en la primera vez fue para registrarse y el campo expires tenía valor 3600 y en esta última es para darse de baja por eso el valor pasa a ser 0.

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

***** Captura de una sesión SIP

  * Dirígete a la web de Linphone (https://www.linphone.org/freesip/home) con el navegador y créate una cuenta SIP.  Recibirás un correo electrónico de confirmación en la dirección que has indicado al registrarte (mira en tu carpeta de spam si no es así).

  * Lanza linphone, y configúralo con los datos de la cuenta que te acabas de crear. Para ello, puedes ir al menú ``Ayuda'' y seleccionar ``Asistente de Configuración de Cuenta''. Al terminar, cierra completamente linphone.

  * Captura una sesión SIP de una conversación con el número SIP sip:music@sip.iptel.org. Recuerda que has de comenzar a capturar tramas antes de arrancar Ekiga para ver todo el proceso.

  * Observa las diferencias en el inicio de la conversación entre el entorno del laboratorio y el del ejercicio anterior:
    * ¿Se utilizan DNS y STUN? ¿Por qué?

      Se utiliza DNS para contactar en primer lugar,

    * ¿Son diferentes el registro y la descripción de la sesión?

      Cambia el nombre del usuario y del SIP.

  * Identifica las diferencias existentes entre esta conversación y la conversación anterior:
    * ¿Cuántos flujos tenemos?

      Dos flujos

    * ¿Cuál es su periodicidad?

      Tiene una preiocidad de 20 * 10^-3 segundos

    * ¿Cuánto es el valor máximo del delta y los valores medios y máximo del jitter?

    * ¿Podrías reproducir la conversación desde Wireshark? ¿Cómo? Comprueba que poniendo un valor demasiado pequeño para el buffer de jitter, la conversación puede no tener la calidad necesaria.

      No he podido

    * ¿Sabrías decir qué tipo de servicio ofrece sip:music@iptel.org?

      Parece ser un servicio al que llamas y ponen musica online, como una especie de radio

  [Al terminar el ejercicio es recomendable hacer commit de los ficheros modificados]

  * Filtra por los paquetes SIP de la captura y guarda *únicamente* los paquetes SIP como p5.pcapng. Abre el fichero guardado para cerciorarte de que lo has hecho bien. Deberás añadirlo al repositorio.

[Al terminar la práctica, realiza un push para sincronizar tu repositorio GitLab]

IMPORTANTE: No olvides rellenar el test de la práctica 5 en el Aula Virtual de la asignatura.
